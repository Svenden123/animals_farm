def sep():
    print(30*'-')


class Animal:
    def __init__(self, name, weight):
        self.name = name
        self.weight = weight

    def speak(self):
        print("{}: Привет!".format(self.name))
        print("{}: Мой вес {}кг.".format(self.name, self.weight))
        sep()

    def action(self, user):
        print("{}: молча смотрит".format(self.name))
        sep()

    def eat(self):
        print("{}: ест".format(self.name))
        sep()


class Me:
    name = 'Mark'

    def say_hello(self,obj):
        print('Я: Привет, {}, как ты?'.format(obj.name))
        obj.speak()

    def ask_action(self, obj):
        print('Я: Давай, {}!'.format(obj.name))
        obj.action(self.name)

    def want_eggs(self, obj):

        print("Я: Мне нужны твои яйца, {}".format(obj.name))

        if type(obj) == Goose or type(obj) == Chicken or type(obj) == Duck:
            obj.give_eggs(self.name)
        else:
            print("Извини, но {} не даст яиц".format(obj.name))

    def feed(self, obj):
        print('Вот немного еды, {}'.format(obj.name))
        obj.eat()

    def ask_heaviest_animal(self, *animals):
        animals_list = []

        for animal in animals:
            animals_list.append([animal.name, animal.weight])

        animals_list.sort(key=lambda i: i[1], reverse=True)

        for i in animals_list:
            print("%20s: %1d" % (i[0], i[1]) + ' кг')

        sep()

        print('Самый тяжелый тут: {}'.format(animals_list[0][0]))


class Goose(Animal):
    def speak(self):
        print('Я гусь {}! Га-га-га'.format(self.name))
        sep()

    def action(self, user):
        print('{} есть траву'.format(self.name))
        sep()

    def give_eggs(self, user):
        print("{} отдаёт яйца {}".format(self.name,user))


class Cow(Animal):
    def speak(self):
        print('Меня зовут {}! Мууууууууу'.format(self.name))
        sep()

    def action(self, user):
        print('{} отдаёт молоко {}'.format(self.name,user))
        sep()


class Goat(Cow, Animal):
    def speak(self):
        print('Бэээээ {}! Я козёл!'.format(self.name))
        sep()


class Sheep(Animal):
    def speak(self):
        print('Мэээээ, мэээээ меня зовут {}! я овца!'.format(self.name))
        sep()

    def action(self, user):
        print('{} побрил {}, теперь есть шерсть'.format(user, self.name))
        sep()


class Chicken(Goose, Animal):
    def speak(self):
        print('Ко-ко-ко меня зовут {}! Я курочка!'.format(self.name))
        sep()


class Duck(Goose, Animal):
    def speak(self):
        print('Кря кря, меня зовут {}! Я уточка!'.format(self.name))
        sep()


human = Animal('Uncle Joe', 80)
gray_goose = Goose('Gray', 5)
white_goose = Goose('White', 7)
cow = Cow('Manya', 300)
goat_horns = Goat('Horns', 10)
goat_hooves = Goat('Hooves', 9)
sheep_barry = Sheep('Barry', 17)
sheep_curvy = Sheep('Curvy', 15)
chicken_koko = Chicken('Koko', 2)
chicken_kuku = Chicken('Kuku', 2.5)
krya_duck = Duck('Krya', 3)

animals = [ human,
            gray_goose,
            white_goose,
            cow,goat_horns,
            goat_hooves,
            sheep_barry,
            sheep_curvy,
            chicken_koko,
            chicken_kuku,
            krya_duck,
            ]

i = Me()

i.ask_heaviest_animal(*animals)

for c,e in enumerate(animals):
    sep()
    i.say_hello(e)
    i.ask_action(e)
    i.want_eggs(e)
    i.feed(e)
